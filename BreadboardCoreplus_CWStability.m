
lambda = 778e-9; % Typical breadboard Core+ CW wavelength

% Distance between curved mirrors: 5.5 cm
% Distance from curved mirror 1 (the one the pump beam passes through
% first) to the end mirror of its arm (the output coupler): (7.5 + 11 + 
% 12.5 + 3*8.5 + 11) cm = 67.5 cm
% Distance from curved mirror 2 (the one the pump beam passes through
% second) to the end mirror of its arm: (15.5 + 16.5 + 13.5) cm = 45.5 cm
% Distance from pump lens to crystal: 4.5 cm

L_arm1 = 67.5e-2; % Distance in arm 1 from curved mirror to end mirror
L_arm2 = 45.5e-2; % Distance in arm 2 from curved mirror to end mirror (output coupler)
L_CM = linspace(4.98e-2,5.25e-2,10000); % Distance between curved mirrors
f = 25e-3; % Guess of curved mirrors' focal length

w_crystal = NaN(length(L_CM),1);
w_end1 = NaN(length(L_CM),1);
w_end2 = NaN(length(L_CM),1);
waistlocation = NaN(length(L_CM),1);

for j=1:length(L_CM)
    % We construct ABCD transfer matrices in three stages. Remember that
    % the first element the beam passes is on the far right (matrix
    % multiplication is not commutative)
    % We can neglect the presence of the crystal, since it is quite thin.
    T_CMmid_end1 = stretch(L_arm1)*lens(f)*stretch(L_CM(j)/2); % From the midpoint between the curved mirrors to the end of arm 1
    T_end1_end2  = stretch(L_arm2)*lens(f)*stretch(L_CM(j))*lens(f)*stretch(L_arm1); % From the end of arm 1 to the end of arm 2 (the output coupler)
    T_end2_CMmid = stretch(L_CM(j)/2)*lens(f)*stretch(L_arm2); % From the end of arm 2 (the output coupler) to the midpoint between the curved mirrors
    T_full = T_end2_CMmid*T_end1_end2*T_CMmid_end1; % The full round trip is the product of all three matrices
    
    q = ABCDfindselfreproducingq(T_full); % This finds a q value that reproduces itself after one full round trip of the cavity, if such a q value exists (otherwise NaN)
    
    if ~isnan(q)
        % We now use that the beam parameter q satisfies
        % q = 1i*pi*w_0^2/(n*lambda) + z
        w_crystal(j) = sqrt(1*lambda*imag(q)/pi);
        waistlocation(j) = real(q);
        q_end1 = ABCDpropagateq(T_CMmid_end1,q); % Real part of this is 0 since focus is necessarily on the mirror
        w_end1(j) = sqrt(1*lambda*imag(q_end1)/pi);
        q_end2 = ABCDpropagateq(T_end1_end2,q_end1); % Real part of this is 0 since focus is necessarily on the mirror
        w_end2(j) = sqrt(1*lambda*imag(q_end2)/pi);
    end
end

figure(2);clf;
subplot(2,2,1);
plot(L_CM/1e-3,w_crystal/1e-6,'Linewidth',2)
xlim([min(L_CM) max(L_CM)]/1e-3);
ylim([0 20]);
grid on
grid minor
xlabel('Distance between curved mirrors [mm]');
ylabel('Crystal waist radius [�m]');
subplot(2,2,2);
plot(L_CM/1e-3,waistlocation/1e-3,'Linewidth',2);
xlim([min(L_CM) max(L_CM)]/1e-3);
ylim([-1 1]);
grid on
grid minor
xlabel('Distance between curved mirrors [mm]');
ylabel('Distance from curved mirror midpoint to crystal waist [mm]');
subplot(2,2,3);
plot(L_CM/1e-3,[w_end1 w_end2]/1e-3,'Linewidth',2)
xlim([min(L_CM) max(L_CM)]/1e-3);
ylim([0 1]);
grid on
grid minor
xlabel('Distance between curved mirrors [mm]');
ylabel('End mirror waist radius [mm]');
legend('End mirror 1 (output coupler)','End mirror 2');
subplot(2,2,4);
plot(L_CM/1e-3,pi*w_end1.^2/lambda,'Linewidth',2)
xlim([min(L_CM) max(L_CM)]/1e-3);
ylim([0 2]);
grid on
grid minor
xlabel('Distance between curved mirrors [mm]');
ylabel('Rayleigh length of output beam [m]');

function q = ABCDfindselfreproducingq(T)
A = T(1,1);B = T(1,2);
C = T(2,1);D = T(2,2);

%   Imposing the requirement that the beam parameter q is unchanged after
%   one full round trip (q = (A*q+B)/(C*q+D)), we arrive at a quadratic
%   equation C*q^2+(D-A)*q-B = 0. We can use the standard formula to solve
%   it. Discriminant must be negative for a physical solution:
d = (D-A)^2+4*C*B;
if d < 0
    % Additionally, the sign to use in front of the square root for a
    % physical solution depends on the sign of C:
    if C>0
        q = (A-D+sqrt(d))/(2*C);
    else
        q = (A-D-sqrt(d))/(2*C);
    end
else
    q = NaN;
end
end

function q_2 = ABCDpropagateq(T,q_1)
q_2 = (T(1,1)*q_1 + T(1,2))/(T(2,1)*q_1 + T(2,2));
end

function T = stretch(L)
T = [ 1 L ; 0 1 ];
end

function T = lens(f)
T = [ 1 0 ; -1/f 1 ];
end

function T = refract(n_1,n_2)
T = [ 1 0 ; 0 n_1/n_2 ];
end